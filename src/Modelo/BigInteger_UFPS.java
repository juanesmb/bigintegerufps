package Modelo;

/**
 *
 * @author madar
 */
public class BigInteger_UFPS 
{
    /**
     *  Numero="23456"
     * miNUmero={2,3,4,5,6};
     */
    private int miNumero[];
    
    
    public BigInteger_UFPS() {
    }

    public BigInteger_UFPS(String miNumero) throws Exception
    {
        if(miNumero.charAt(0)=='-')
            throw new Exception("negativo. No se puede calcular el factorial de 0 o negativo");
        if(miNumero.length()==1 && miNumero.charAt(0)=='0')
            throw new Exception("Cero. No se puede calcular el factorial de 0 o negativo");
        
        
        String digitos[] = miNumero.split("");
        int dig[] = new int[digitos.length];
        for (int i = 0; i < digitos.length; i++) 
            dig[i] = Integer.parseInt(digitos[i]);

        this.miNumero=dig;
    }
    
    public BigInteger_UFPS(int [] miNumero) 
    {
        this.miNumero=miNumero;
    }

    public int[] getMiNumero() {
        return miNumero;
    }
    
    
    /**
     * Mutiplica dos enteros BigInteger
     * @param dos
     * @return 
     */
    
    public BigInteger_UFPS multiply(BigInteger_UFPS dos)
    {
        int [] r = new int[this.miNumero.length+dos.miNumero.length+1];
        int [] miNumeroInvertido = invertirArreglo(this.miNumero);
        int [] dosInvertido = dos.invertirArreglo(dos.miNumero);
        
        for (int i = 0; i < dos.miNumero.length; i++) 
        {
            for (int j = 0; j < this.miNumero.length; j++) 
            {
                int mult = (dosInvertido[i]*miNumeroInvertido[j]) + r[i+j];
                r[i+j] = mult%10;
                r[i+j+1] = r[i+j+1] + mult/10 ;
            }
        }
        
        r = invertirArreglo(r);
        r = normalizarArreglo(r);
        return new BigInteger_UFPS(r);
    }   
    
    private int[] invertirArreglo(int[] arreglo)
    {
        int [] r = new int [arreglo.length];
        int cont = 0;
        for (int i = arreglo.length-1; i>=0 ;i--) 
        {
            r[cont] = arreglo[i];
            cont++;
        }
        return r;
    }
    
    private int[] normalizarArreglo(int[] arreglo) 
    {
        int posicion = 0;
        
        for (int i = 0; i < arreglo.length; i++) 
        {
            if(arreglo[i] != 0)
            {
                posicion = i;
                break;
            } 
        }
        int [] r = new int [arreglo.length-posicion];
        int cont = 0;
        for (int i = posicion; i < arreglo.length; i++) 
        {
            r[cont] = arreglo[i];
            cont++;
        }
        return r;
    }
         
    
    /**
     * Retorna la representación entera del BigInteger_UFPS
     * @return un entero
     */
    public int intValue()
    {
        int numero = 0;
        int decimal = 1;
        for (int i = this.miNumero.length-1; i >= 0; i--) 
        {
            numero = numero + this.miNumero[i]*decimal;
            decimal *= 10;
        }
        return numero;
    }

    @Override
    public String toString() 
    {
        String msg = "";
        for (int i = 0; i < this.miNumero.length; i++) 
            msg = msg + this.miNumero[i];
        
        return msg;
    }
}